# Raman spectra with BigDFT

The goal of this repository is to contain all the necessary data to reproduce
the work I did to compute the convergence of the Raman spectrum using BigDFT,
and also to compare my results to those of the literature.

The main results of the study were presented at the 2019 APS March Meeting in
Boston. The files used to create the pdf support used at that time are stored
in the `MarchMeetingPresentation` directory.


## What do we have here?

### What is the goal of this project?

This repo contains all the necessary data to reproduce the work on the 
computation of Raman spectra (and other quantities) from first principles using
the BigDFT code. A test set of molecules, for which experimental and
theoretical data were profuse, is used in this study. 

One of the main goal of this project was to study how the quality of the
wavelet grid, used by the BigDFT code to represent the electronic wavefunctions
of the system, impacts the quality of the results, whether it be Raman or
infrared spectra, vibrational and electronic polarizability, to name a few
quantities of physical interest.

Performing a convergence study for each study allowed us to find suitable input
parameters to consider the computation as converged. We used the precision per
atom on the total energy of the system as the driving quantity. These results
were later used to define an error bar for each quantity.


### How to go through this repo?

The repo is mainly a collection of python notebooks, which are used to perform
every task in order to create a database of results for a test set of small
molecules. 

These notebooks are supposed to be run in that order:
- `experimental_and_theoretical_csv_files.ipynb` creates and documents all the
relevant csv files containing the experimental and theoretical data found in
the literature.
- `prepare_notebooks.ipynb` prepares all the notebooks used to perform the
study.
    * Each notebook is created from an xyz file and the value of the BigDFT
      parameters `rmult` and `hgrids` (defining the real-space grids used to
      represent the system's wavefunctions).
    * These notebooks can then be run to compute all the relevant quantities
      studied here, especially the Raman spectrum (which is the most
      time-consuming part), but also the infrared spectrum and the mean
      electronic and vibrational polarizabilities.
    * All these notebooks heavily depend on the MyBigDFT library. The latter
      provides a (hopefully) clear, concise and meaningful API to manipulate
      the BigDFT code with Python.
- OPTIONAL: `run_all_notebooks.ipynb` runs all the notebooks created by the
`prepare_notebooks.ipynb` notebook. An equivalent is the `run_notebooks.sh`
file. You may not have to run this notebook or this script if you have access
to the output notebooks, hence the OPTIONAL mention above. These files might be
included in the project in the future (in an archive); for the moment, they can
be provided upon request.
- `database_creation.ipynb` is currently used to create, document, fill
and use the database of BigDFT results as well as experimental and theoretical
results to be used as comparison.
    * sqlite3 is used to manage the database.
    * The BigDFT results are read from the output of the notebooks created by
      the `prepare_notebooks.ipynb` notebook.
    * These results are added to the database.
    * The tables are also written in csv files.
    * Experimental and theoretical data (stored in csv files) are also added to
      the database.

You must also take a look at the `convergence_studies` folder. It contains the
`post_processing.ipynb` notebook, that you have to run to make sure that the
database contains the data regarding the converged parameters for each system.

You may also want to look at the `README.md` file in that directory to have a
more detailed view of what it contains; for the moment, you may only have to
know that this directory contains all the relevant notebooks to perform the
convergence studies for all the molecules in the test set with BigDFT. The
main objective of these studies is to define the BigDFT input parameters
(namely the grid step and the grid extension) that minimize the computational
time while preserving an accuracy within 1 meV per atom with respect to a
reference calculation. These input parameters can then be used to discriminate
between "converged" and "non-converged" BigDFT calculations.

The data visualization of all the BigDFT results is presented in the
`data_visualization` folder. One notebook per quantity of interest is present
there. Their main objective is to evaluate the expected precision on the
quantity when the BigDFT input parameters total energy is precise up to at most
2 meV per atom. Many plots and stats are produced so as to help defining and
visualizing that precision.

Some initial data files are also to be found:
- the initial positions of the studied molecules are found in the
`initial_positions` directory in the form of BigDFT-compliant xyz files,
- the reference data (both experimental and theoretical) are found in the
`data_from_literature` directory in the form of csv files. These files actually
are redundant with the `experimental_and_theoretical_csv_files.ipynb` notebook,
which was used to create them.
- the `data_from_BigDFT` directory, which contains csv files that gather all
the relevant data found as output of the BigDFT calculations defined in the
`prepare_notebooks.ipynb` notebook.

Finally, as already stated, the `MarchMeetingPresentation` directory contains
all the necessary data to compile the main.tex file, which is nothing but a
beamer presentation.


## Getting started

Install jupyter to run all the notebooks. They might use features only present
in python 3.6+ (mainly the so-called f-strings), so make sure you have
the correct python version installed and that the correct python
kernel is used by jupyter as well.

Another requirement is to install the
[MyBigDFT](https://gitlab.com/mmoriniere/MyBigDFT) library, that was used
extensively during this project. It allows one to interact with the BigDFT code
via a clear and easy to use Python interface.

You can install the BigDFT code as well if you wish to re-run the calculations.
If you would rather use the data contained in this repo without having to run a
BigDFT job, you still need to provide a path to the
`input_variables_definition.yaml` file of the BigDFT 1.8.3 release. MyBigDFT
uses the `$BIGDFT_SOURCES` environment variable to access it: `$BIGDFT_SOURCES`
must be a path leading to the directory where the source directory of BigDFT
is.

There are two options to install the BigDFT suite:
- if you have access to all the BigDFT output files I created during my study,
install 1.8.3 version. Some of these output files were obtained with the
BigDFT 1.8.3 version (even though most were obtained with the 1.8.2 version).
Given that some input variables changed between both releases, an error is
raised when the BigDFT results are stored in the database in the
`database_creation.ipynb` notebook.
- if you actually want to run all the calculations by yourself, you may use any
other BigDFT version, but it is recommended to use BigDFT 1.8.2 and higher, as
the MyBigDFT library was developped with BigDFT 1.8.2 and used successfully
with the 1.8.3 version as well.


## Notes

If you wish to re-run all the BigDFT jobs but want to skip the geometry
optimization part for each setup, you may want to use the provided 
`final_*.xyz` files provided for each setup. This means that you will have to
modify the `prepare_notebooks.ipynb` notebook:
- first, skip or delete the part about the geometry optimization,
- second, provide the input positions of the corresponding `final_*.xyz` file
to the ground state of all the workflows that need to start from a relaxed
configuration. It includes the ones computing the phonons and the
polarizability tensor.


## Notes to myself and todos:

- Use the database or the data folders in order to "DRY" the notebooks,
expecially when it comes to define the values of the BigDFT input parameters.
- It might be nice to use repo2docker to create a Docker image I can then share.
