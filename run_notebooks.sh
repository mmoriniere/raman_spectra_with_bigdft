for rm in 5_8 6_9 7_10 8_11 9_12
do
    echo $rm
    for hg_dec in 16 20 24 28 32 36 40
    do
        hg=0.$hg_dec
        echo $hg
        for mol in "CH4" "N2" "NH3" "CO" "H2O" "HCN" "HCCH" "H3CCN" "H3CNC" "NF3" "SiF4"
        do
            fol=rm_$rm/hg_$hg
            echo fol=$fol
            notebook_to_run=$fol/$mol.ipynb
            ls -ltra $notebook_to_run
            notebook_finished=$fol/$mol.nbconvert.ipynb
            if [ -f $notebook_finished ]; then
                echo "Notebook $notebook_to_run already ran successfully"
            else
                jupyter nbconvert --execute --to notebook --ExecutePreprocessor.timeout=72000 $notebook_to_run
            fi
        done
    done
done
