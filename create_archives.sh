# This script is meant to create the archives containing all the relevant data
# used to fill the database. Five types of archives are built here:
# - the first one contains all the notebooks that ran successfully, which
#   contains the data regarding the phonons energies, the Raman and infrared
#   intensities, and the mean electronic and vibrational polarizability.
# - the second one contains all the geopt logfiles, which contains the data
#   regarding the total energy and the dipoles of the ground state of the
#   system for each setup.
# - the third one is meant to store the logfiles used to compute the phonon
#   energies, infrared intensities and vibrational polarizability. One such
#   archive per molecule is created, as it is not possible to store all these
#   files in one archive.
# - the fourth one is similar as the one above, as it contains logfiles used to
#   compute the Raman intensities. For a similar reason, there has to be a
#   six archives per molecule.
# - the fifth one contains all the logfiles allowing to compute the electronic
#   polarizability tensor for all the molecules.

ARCH_DIR="archives"

####################################################
# Create an archive made of the notebooks that ran #
####################################################
# It should actually be as simple as:
# tar -czvf run_notebooks.tar.gz rm_*/hg_*/*.nbconvert.ipynb
# but this is not the case, since we have not been able to get the Raman
# intensities correctly the first time we ran all the calculations. To keep
# the other data (phonon, infrared, polarizabilities) were correct, we copied
# the notebooks with bad Raman intensities in other files with a ".old" suffix.

# Define a tar filename and delete it if it exists (so as to avoid having the
# same file multiple times in the archive)
tar_file=$ARCH_DIR/"run_notebooks.tar"
if [ -f $tarfile ]
then
    rm $tarfile
fi
# Loop over the files to be added to the archive
for rmult_dir in rm_*
do
    echo $rmult_dir
    for setup_dir in $rmult_dir/hg_*
    do
        echo $setup_dir
        for posinp in $(basename initial_positions/*.xyz)
        do
            molecule=${posinp%.*}
            # Expected path to the notebook that ran
            run_notebook=$setup_dir/$molecule.nbconvert.ipynb
            # Path to the old notebook that ran
            old_run_notebook=$run_notebook.old
            # Look for the expected notebook and add it to the archive
            if [ -f $run_notebook ]
            then
                echo $run_notebook exists
                tar -rvf $tar_file $run_notebook
            # Look for the old notebook and add it to the archive
            elif [ -f $old_run_notebook ]
            then
                echo $old_run_notebook exists
                tar -rvf $tar_file $old_run_notebook
            # One of both files must be present
            else
                echo "Missing file"
                break
            fi
        done
    done
done
# End the whole thing by gzipping the archive to save memory (removes $tarfile
# and creates a file named $tarfile".gz")
gzip -f $tar_file


################################################
# Create an archive made of the geopt logfiles #
################################################

tar -czvf $ARCH_DIR/geopt_logfiles.tar.gz rm_*/hg_*/*/geopt/log-*.yaml


##################################################
# Create an archive made of the phonons logfiles #
##################################################

for posinp in $(basename initial_positions/*.xyz)
do
    molecule=${posinp%.*}
    tar -czvf $ARCH_DIR/"phonons_"$molecule"_logfiles.tar.gz" rm_*/hg_*/$molecule/phonons/atom*/*/log-*.yaml
done

############################################################
# Create an archive made of the Raman intensities logfiles #
############################################################

for posinp in $(basename initial_positions/*.xyz)
do
    molecule=${posinp%.*}
    for EF_direction in "x+" "y+" "z+" "x-" "y-" "z-"
    do
        tar -czvf $ARCH_DIR/"raman_"$molecule"_EF_along_"$EF_direction"logfiles.tar.gz" rm_*/hg_*/$molecule/phonons/atom*/*/EF_along_$EF_direction/log-*.yaml
    done
done


####################################################################
# Create an archive made of the electronic polarizability logfiles #
####################################################################

tar -czvf $ARCH_DIR/elec_pol_logfiles.tar.gz rm_*/hg_*/*/pol_tensor/EF_along_*/log-*.yaml rm_*/hg_*/*/pol_tensor/log-*.yaml
