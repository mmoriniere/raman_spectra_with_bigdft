for mol in H2O N2 CO CH4 SiF4 HCN HCCH H3CNC H3CCN NF3 NH3
do
    echo $mol
    jupyter nbconvert --to notebook --execute $mol.ipynb
done
